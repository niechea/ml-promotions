<?php

/**
 * @file
 * Code for the promotions feature.
 */

include_once 'ml_promotions.features.inc';

/**
 * Implements hook_theme().
 */
function ml_promotions_theme() {
  return array(
    'promotion' => array (
      'render element' => 'elements',
      'path' => drupal_get_path('module', 'ml_promotions') . '/theme/templates',
      'template' => 'promotion'
    ),
    'promotion__image' => array (
      'path' => drupal_get_path('module', 'ml_promotions') . '/theme/templates',
      'base hook' => 'promotion',
      'template' => 'promotion--image'
    ),
    'promotion__text' => array (
      'path' => drupal_get_path('module', 'ml_promotions') . '/theme/templates',
      'base hook' => 'promotion',
      'template' => 'promotion--text'
    ),
  );
}

/**
 * Implements hook_menu().
 */
function ml_promotions_menu() {
  $items = array();

  $items['admin/content/promotion'] = array(
    'title' => 'Promotions',
    'page callback' => 'views_embed_view',
    'page arguments' => array('ml_promotions_admin'),
    'access arguments' => array('administer promotion entities'),
    'type' => MENU_LOCAL_TASK | MENU_NORMAL_ITEM,
  );

  $items['admin/content/promotion/list'] = array(
    'title' => 'List',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items['promotion/add'] = array(
    'title' => 'Add promotion',
    'page callback' => 'ml_promotions_admin_add_page',
    'access arguments' => array('administer promotion entities'),
    'file' => 'ml_promotions.admin.inc',
  );

  foreach (ml_promotions_types() as $type => $info) {
    $items['promotion/add/' . $type] = array(
      'title' => 'Add ' . $type . ' promotion',
      'page callback' => 'ml_promotions_add',
      'page arguments' => array(2),
      'access callback' => 'entity_access',
      'access arguments' => array('create', 'promotion', $type),
      'file' => 'ml_promotions.admin.inc',
    );
  }

  $items['promotion/%promotion'] = array(
    'title callback' => 'entity_class_label',
    'title arguments' => array(1),
    'page callback' => 'promotion_view_page',
    'page arguments' => array(1),
    'access callback' => 'entity_access',
    'access arguments' => array('edit', 'promotion', 1),
    'file' => 'ml_promotions.admin.inc',
  );

  $items['promotion/%promotion/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items['promotion/%promotion/edit'] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('promotion_form', 1),
    'access callback' => 'entity_access',
    'access arguments' => array('edit', 'promotion', 1),
    'file' => 'ml_promotions.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 0,
  );

  $items['promotion/%promotion/delete'] = array(
    'title' => 'Delete promotion',
    'title callback' => 'entity_class_label',
    'title arguments' => array(1),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('promotion_delete_confirm_form', 1),
    'access callback' => 'entity_access',
    'access arguments' => array('edit', 'promotion', 1),
    'file' => 'ml_promotions.admin.inc',
  );

  $items['admin/structure/promotion-types/%promotion_type/delete'] = array(
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('promotion_type_delete_confirm_form', 3),
    'access arguments' => array('administer promotion types'),
    'weight' => 1,
    'type' => MENU_NORMAL_ITEM,
    'file' => 'ml_promotions.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_menu_local_tasks_alter().
 */
function ml_promotions_menu_local_tasks_alter(&$data, $router_item, $root_path) {
  // Add action link to 'promotion/add' on 'admin/content/promotion' page.
  if ($root_path == 'admin/content/promotion') {
    $item = menu_get_item('promotion/add');
    if (!empty($item['access'])) {
      $data['actions']['output'][] = array(
        '#theme' => 'menu_local_action',
        '#link' => $item,
        '#weight' => $item['weight'],
      );
    }
  }
}

/**
 * Implements hook_permission().
 */
function ml_promotions_permission() {
  $permissions = array(
    'administer promotion types' => array(
      'title' => t('Administer promotion types'),
      'description' => t('Allows users to configure promotion types and their fields.'),
      'restrict access' => TRUE,
    ),
    'create promotion entities' => array(
      'title' => t('Create promotions'),
      'description' => t('Allows users to create promotions.'),
      'restrict access' => TRUE,
    ),
    'view promotion entities' => array(
      'title' => t('View promotions'),
      'description' => t('Allows users to view promotions.'),
      'restrict access' => TRUE,
    ),
    'edit any promotion entities' => array(
      'title' => t('Edit any promotions'),
      'description' => t('Allows users to edit any promotions.'),
      'restrict access' => TRUE,
    ),
    'edit own promotion entities' => array(
      'title' => t('Edit own promotions'),
      'description' => t('Allows users to edit own promotions.'),
      'restrict access' => TRUE,
    ),
  );

  return $permissions;
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function ml_promotions_ctools_plugin_directory($module, $plugin) {
  if ($module == 'ctools' && !empty($plugin)) {
    return "plugins/$plugin";
  }
}

/**
 * Implements hook_entity_info().
 */
function ml_promotions_entity_info() {
  $return = array(
    'promotion' => array(
      'label' => t('Promotion'),
      'entity class' => 'promotion',
      'controller class' => 'promotionController',
      'base table' => 'promotion',
      'fieldable' => TRUE,
      'entity keys' => array(
        'id' => 'pid',
        'bundle' => 'type',
      ),
      'bundle keys' => array(
        'bundle' => 'type',
      ),
      'bundles' => array(),
      'load hook' => 'promotion_load',
      'view modes' => array(
        'full' => array(
          'label' => t('Default'),
          'custom settings' => TRUE,
        ),
      ),
      'uri callback' => 'entity_class_uri',
      'label callback' => 'entity_class_label',
      'access callback' => 'promotion_access',
      'module' => 'ml_promotions',
    ),
    'promotion_type' => array(
      'label' => t('Promotion Type'),
      'entity class' => 'promotionType',
      'controller class' => 'promotionTypeController',
      'base table' => 'promotion_type',
      'fieldable' => FALSE,
      'bundle of' => 'promotion',
      'exportable' => TRUE,
      'entity keys' => array(
        'id' => 'id',
        'name' => 'type',
        'label' => 'label',
      ),
      // Enable the entity API's admin UI.
      'admin ui' => array(
        'path' => 'admin/structure/promotion-types',
        'file' => 'ml_promotions.admin.inc',
        'controller class' => 'promotionTypeUIController',
      ),
      'access callback' => 'promotion_type_access',
      'module' => 'ml_promotions',
    ),
  );

  return $return;
}

/**
 * Implements hook_entity_info_alter().
 */
function ml_promotions_entity_info_alter(&$entity_info) {
  foreach (ml_promotions_types() as $type => $info) {
    $entity_info['promotion']['bundles'][$type] = array(
      'label' => $info->label,
      'admin' => array(
        'path' => 'admin/structure/promotion-types/manage/%promotion_type',
        'real path' => 'admin/structure/promotion-types/manage/' . $type,
        'bundle argument' => 4,
      ),
    );
  }
}

/**
 * Implements hook_entity_property_info_alter().
 */
function ml_promotions_entity_property_info_alter(&$info) {
  $properties = &$info['promotion']['properties'];

  $properties['created'] = array(
    'label' => t('Date created'),
    'type' => 'date',
    'description' => t('The date the promotion was posted.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer promotion entities',
    'schema field' => 'created',
  );

  $properties['changed'] = array(
    'label' => t('Date changed'),
    'type' => 'date',
    'schema field' => 'changed',
    'description' => t('The date the promotion was most recently updated.'),
  );

  $properties['uid'] = array(
    'label' => t('Author'),
    'type' => 'user',
    'description' => t('The author of the promotion.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer promotion entities',
    'required' => TRUE,
    'schema field' => 'uid',
  );
}

/**
 * Implements hook_admin_paths().
 */
function ml_promotions_admin_paths() {
  return array(
    'promotion/add' => TRUE,
    'promotion/add/*' => TRUE,
    'promotion/*' => TRUE,
    'promotion/*/edit' => TRUE,
    'promotion/*/delete' => TRUE,
  );
}

/**
 * Returns ISO-8601 numeric representation of the days of the week.
 * 1 (for Monday) through 7 (for Sunday).
 */
function ml_promotions_days_of_the_week() {
  return array(
    1 => t('Monday'),
    2 => t('Tuesday'),
    3 => t('Wednesday'),
    4 => t('Thursday'),
    5 => t('Friday'),
    6 => t('Saturday'),
    7 => t('Sunday'),
  );
}

/**
 * Preprocess variables for the promotion.tpl.php.
 */
function template_preprocess_promotion(&$variables) {
  $promotion = entity_metadata_wrapper('promotion', $variables['elements']['#entity']);

  // Template suggestions.
  $variables['theme_hook_suggestions'][] = 'promotion__' . $promotion->type->value();
  $variables['theme_hook_suggestions'][] = 'promotion__' . $promotion->pid->value();

  // Attributes.
  $variables['attributes_array']['class'][] = 'Promotion';
  $variables['attributes_array']['class'][] = 'Promotion--' . drupal_html_class($promotion->title->value());
  $variables['attributes_array']['class'][] = 'Promotion--' . $promotion->type->value();
  $variables['attributes_array']['class'][] = 'Promotion--' . ($promotion->status->value() ? '' : 'un') . 'published';

  // URL attributes.
  $variables['link'] = $promotion->url->value();
  $variables['link_attributes_array']['href'] = url($promotion->url->value());
  $variables['link_attributes_array']['data-promotion-pid'] = $promotion->pid->value();
  $variables['link_attributes_array']['data-promotion-title'] = $promotion->title->value();

  // Promotion content.
  $variables['cta'] = $promotion->cta->value();

  switch ($promotion->type->value()) {
    case 'image':
      $image = $promotion->field_promotion_image->value();
      $variables['image'] = theme('image', array(
        'path' => $image['uri'],
        'alt' => $image['alt'],
        'attributes' => array(
          'class' => array('Promotion-image'),
        ),
      ));
      break;

    case 'text':
      $variables['title'] = $promotion->title->value();
      $variables['body'] = $promotion->field_promotion_body->value();
      break;
  }
}

/**
 * Process variables for the promotion.tpl.php.
 */
function template_process_promotion(&$variables) {
  $variables['link_attributes'] = drupal_attributes($variables['link_attributes_array']);
}

/**
 * Preprocess variables for the box.tpl.php.
 */
function ml_promotions_preprocess_panels_pane(&$variables) {
  if ($variables['pane']->type == 'promotion') {
    $variables['attributes_array']['class'][] = 'Box--promotion-' . $variables['content']['#entity']->type;
    $variables['attributes_array']['class'][] = 'Box--promotion-' . drupal_html_class($variables['content']['#entity']->title);
  }
}

/**
 * Page callback for /promotion/%promotion.
 */
function promotion_view_page($promotion) {
  return promotion_view($promotion, 'preview');
}

/**
 * Promotion API ==================================================================
 */

/**
 * Access callback for promotion.
 */
function promotion_access($op, $promotion, $account = NULL, $entity_type = NULL) {
  global $user;

  if (!isset($account)) {
    $account = $user;
  }

  switch ($op) {
    case 'create':
      return user_access('administer promotion entities', $account)
          || user_access('create promotion entities', $account);

    case 'view':
      if (!$promotion->status || (!empty($promotion->days) && !array_key_exists(date('N'), $promotion->days))) {
        return FALSE;
      }

      return user_access('administer promotion entities', $account)
          || user_access('view promotion entities', $account);

    case 'edit':
      return user_access('administer promotion entities')
          || user_access('edit any promotion entities')
          || (user_access('edit own promotion entities') && ($promotion->uid == $account->uid));
  }
}

/**
 * View multiple promotions.
 */
function promotion_view_multiple($promotions, $view_mode = 'full', $weight = 0, $langcode = NULL) {
  field_attach_prepare_view('promotion', $promotions, $view_mode, $langcode);
  entity_prepare_view('promotion', $promotions, $langcode);

  $build = array();

  foreach ($promotions as $promotion) {
    if ($output = promotion_view($promotion, $view_mode, $langcode)) {
      $build['promotions'][$promotion->pid] = $output;
      $build['promotions'][$promotion->pid]['#weight'] = $weight++;
    }
  }

  $build['promotions']['#sorted'] = TRUE;
  return $build;
}

/**
 * View promotion.
 */
function promotion_view($promotion, $view_mode = 'full', $langcode = NULL) {
  if ($view_mode != 'preview' && !promotion_access('view', $promotion)) {
    return FALSE;
  }

  if (!isset($langcode)) {
    $langcode = $GLOBALS['language_content']->language;
  }

  // Populate $promotion->content with a render array.
  promotion_build_content($promotion, $view_mode, $langcode);

  $build = $promotion->content;
  unset($promotion->content);

  $build += array(
    '#theme' => 'promotion',
    '#entity' => $promotion,
    '#view_mode' => $view_mode,
    '#language' => $langcode,
  );

  // Allow modules to modify the structured reward.
  $type = 'promotion';
  drupal_alter(array('promotion_view', 'entity_view'), $build, $type);

  return $build;
}

/**
 * Builds a structured array representing the promotion's content.
 */
function promotion_build_content($promotion, $view_mode = 'full', $langcode = NULL) {
  if (!isset($langcode)) {
    $langcode = $GLOBALS['language_content']->language;
  }

  // Remove previously built content, if exists.
  $promotion->content = array();

  // Allow modules to change the view mode.
  $context = array(
    'entity_type' => 'promotion',
    'entity' => $promotion,
    'langcode' => $langcode,
  );
  drupal_alter('entity_view_mode', $view_mode, $context);

  // Build fields content. In case of a multiple view, reward_view_multiple()
  // already ran the 'prepare_view' step. An internal flag prevents the
  // operation from running twice.
  field_attach_prepare_view('promotion', array($promotion->pid => $promotion), $view_mode, $langcode);
  entity_prepare_view('promotion', array($promotion->pid => $promotion), $langcode);
  $promotion->content += field_attach_view('promotion', $promotion, $view_mode, $langcode);

  // Allow modules to make their own additions to the promotion.
  module_invoke_all('promotion_view', $promotion, $view_mode, $langcode);
  module_invoke_all('entity_view', $promotion, 'promotion', $view_mode, $langcode);

  // Make sure the current view mode is stored if no module has already
  // populated the related key.
  $promotion->content += array('#view_mode' => $view_mode);
}

/**
 * Load a promotion.
 */
function promotion_load($pid, $reset = FALSE) {
  $promotions = promotion_load_multiple(array($pid), array(), $reset);
  return reset($promotions);
}

/**
 * Load multiple promotions.
 */
function promotion_load_multiple($pids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('promotion', $pids, $conditions, $reset);
}

/**
 * Save promotion.
 */
function promotion_save($promotion) {
  entity_save('promotion', $promotion);
}

/**
 * Delete single promotion.
 */
function promotion_delete($promotion) {
  entity_delete('promotion', entity_id('promotion', $promotion));
}

/**
 * Delete multiple promotions.
 */
function promotion_delete_multiple($pids) {
  entity_delete_multiple('promotion', $pids);
}

/**
 * Promotion Type API =============================================================
 */

/**
 * Access callback for promotion Type.
 */
function promotion_type_access($op, $entity = NULL) {
  return user_access('administer promotion types');
}

/**
 * Load promotion Type.
 */
function promotion_type_load($promotion_type) {
  return ml_promotions_types($promotion_type);
}

/**
 * List of promotion Types.
 */
function ml_promotions_types($type_name = NULL) {
  $types = entity_load_multiple_by_name('promotion_type', isset($type_name) ? array($type_name) : FALSE);
  return isset($type_name) ? reset($types) : $types;
}

/**
 * Save promotion Type.
 */
function promotion_type_save($promotion_type) {
  entity_save('promotion', $promotion_type);
}

/**
 * Delete single promotion Type.
 */
function promotion_type_delete($promotion_type) {
  entity_delete('promotion_type', entity_id('promotion_type', $promotion_type));
}

/**
 * Delete multiple promotion Types.
 */
function promotion_type_delete_multiple($rtids) {
  entity_delete_multiple('promotion_type', $rtids);
}
