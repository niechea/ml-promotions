<div<?php print $attributes; ?>>
  <?php if ($link): ?><a<?php print $link_attributes; ?>><?php endif; ?>
    <?php if ($cta): ?>
      <div class="Promotion-cta Button"><?php print $cta; ?></div>
    <?php endif; ?>
  <?php if ($link): ?></a><?php endif; ?>
</div>
