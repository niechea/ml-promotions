<div<?php print $attributes; ?>>
  <div class="PromotionGroup-progress"><div class="Progress-inner"></div></div>

  <?php if ($show_controls): ?>
  <div class="PromotionGroup-controls">
    <div class="PromotionGroup-prev Icon--angleLeft"></div>
    <div class="PromotionGroup-pager">
      <?php foreach ($promotions as $promotion): ?>
        <span class="Icon--circle"></span>
      <?php endforeach; ?>
    </div>
    <div class="PromotionGroup-next Icon--angleRight"></div>
  </div>
  <?php endif; ?>

  <?php foreach ($promotions as $promotion): ?>
    <?php print render($promotion); ?>
  <?php endforeach; ?>
</div>
